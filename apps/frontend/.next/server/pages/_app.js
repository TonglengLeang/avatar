/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./Layout/index.tsx":
/*!**************************!*\
  !*** ./Layout/index.tsx ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"LayouWrapper\": () => (/* binding */ LayouWrapper)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n\nconst LayouWrapper = (styled_components__WEBPACK_IMPORTED_MODULE_0___default().div)`\n  background-color: #cdcdcd;\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9MYXlvdXQvaW5kZXgudHN4LmpzIiwibWFwcGluZ3MiOiI7Ozs7OztBQUF1QztBQUVoQyxNQUFNQyxlQUFlRCw4REFBVSxDQUFDOztBQUV2QyxDQUFDLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9MYXlvdXQvaW5kZXgudHN4PzczODUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmV4cG9ydCBjb25zdCBMYXlvdVdyYXBwZXIgPSBzdHlsZWQuZGl2YFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2RjZGNkO1xuYDtcbiJdLCJuYW1lcyI6WyJzdHlsZWQiLCJMYXlvdVdyYXBwZXIiLCJkaXYiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./Layout/index.tsx\n");

/***/ }),

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! Layout */ \"./Layout/index.tsx\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var theme__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! theme */ \"./theme/index.tsx\");\n/* harmony import */ var theme_themeConfig__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! theme/themeConfig */ \"./theme/themeConfig.ts\");\n/* harmony import */ var _styles_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./styles.css */ \"./pages/styles.css\");\n/* harmony import */ var _styles_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_css__WEBPACK_IMPORTED_MODULE_6__);\n\n\n\n\n\n\n\nfunction CustomApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(styled_components__WEBPACK_IMPORTED_MODULE_3__.ThemeProvider, {\n        theme: theme_themeConfig__WEBPACK_IMPORTED_MODULE_5__.ThemeConfig,\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"meta\", {\n                    name: \"viewport\",\n                    content: \"width=device-width, initial-scale=1, maximum-scale=1\"\n                }, void 0, false, {\n                    fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n                    lineNumber: 13,\n                    columnNumber: 9\n                }, this)\n            }, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n                lineNumber: 12,\n                columnNumber: 7\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(theme__WEBPACK_IMPORTED_MODULE_4__.GlobalStyle, {}, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n                lineNumber: 18,\n                columnNumber: 7\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Layout__WEBPACK_IMPORTED_MODULE_1__.LayouWrapper, {\n                children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                    ...pageProps\n                }, void 0, false, {\n                    fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n                    lineNumber: 20,\n                    columnNumber: 9\n                }, this)\n            }, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n                lineNumber: 19,\n                columnNumber: 7\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/_app.tsx\",\n        lineNumber: 11,\n        columnNumber: 5\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CustomApp);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUFzQztBQUVUO0FBQ3FCO0FBQ2Q7QUFDWTtBQUMxQjtBQUV0QixTQUFTSyxVQUFVLEVBQUVDLFVBQVMsRUFBRUMsVUFBUyxFQUFZLEVBQUU7SUFDckQscUJBQ0UsOERBQUNMLDREQUFhQTtRQUFDTSxPQUFPSiwwREFBV0E7OzBCQUMvQiw4REFBQ0gsa0RBQUlBOzBCQUNILDRFQUFDUTtvQkFDQ0MsTUFBSztvQkFDTEMsU0FBUTs7Ozs7Ozs7Ozs7MEJBR1osOERBQUNSLDhDQUFXQTs7Ozs7MEJBQ1osOERBQUNILGdEQUFZQTswQkFDWCw0RUFBQ007b0JBQVcsR0FBR0MsU0FBUzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJaEM7QUFFQSxpRUFBZUYsU0FBU0EsRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzL19hcHAudHN4PzJmYmUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTGF5b3VXcmFwcGVyIH0gZnJvbSAnTGF5b3V0JztcbmltcG9ydCB7IEFwcFByb3BzIH0gZnJvbSAnbmV4dC9hcHAnO1xuaW1wb3J0IEhlYWQgZnJvbSAnbmV4dC9oZWFkJztcbmltcG9ydCB7IFRoZW1lUHJvdmlkZXIgfSBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5pbXBvcnQgeyBHbG9iYWxTdHlsZSB9IGZyb20gJ3RoZW1lJztcbmltcG9ydCB7IFRoZW1lQ29uZmlnIH0gZnJvbSAndGhlbWUvdGhlbWVDb25maWcnO1xuaW1wb3J0ICcuL3N0eWxlcy5jc3MnO1xuXG5mdW5jdGlvbiBDdXN0b21BcHAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9OiBBcHBQcm9wcykge1xuICByZXR1cm4gKFxuICAgIDxUaGVtZVByb3ZpZGVyIHRoZW1lPXtUaGVtZUNvbmZpZ30+XG4gICAgICA8SGVhZD5cbiAgICAgICAgPG1ldGFcbiAgICAgICAgICBuYW1lPVwidmlld3BvcnRcIlxuICAgICAgICAgIGNvbnRlbnQ9XCJ3aWR0aD1kZXZpY2Utd2lkdGgsIGluaXRpYWwtc2NhbGU9MSwgbWF4aW11bS1zY2FsZT0xXCJcbiAgICAgICAgLz5cbiAgICAgIDwvSGVhZD5cbiAgICAgIDxHbG9iYWxTdHlsZSAvPlxuICAgICAgPExheW91V3JhcHBlcj5cbiAgICAgICAgPENvbXBvbmVudCB7Li4ucGFnZVByb3BzfSAvPlxuICAgICAgPC9MYXlvdVdyYXBwZXI+XG4gICAgPC9UaGVtZVByb3ZpZGVyPlxuICApO1xufVxuXG5leHBvcnQgZGVmYXVsdCBDdXN0b21BcHA7XG4iXSwibmFtZXMiOlsiTGF5b3VXcmFwcGVyIiwiSGVhZCIsIlRoZW1lUHJvdmlkZXIiLCJHbG9iYWxTdHlsZSIsIlRoZW1lQ29uZmlnIiwiQ3VzdG9tQXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIiwidGhlbWUiLCJtZXRhIiwibmFtZSIsImNvbnRlbnQiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/_app.tsx\n");

/***/ }),

/***/ "./theme/darkTheme.ts":
/*!****************************!*\
  !*** ./theme/darkTheme.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n\nconst darkTheme = styled_components__WEBPACK_IMPORTED_MODULE_0__.css`\n  body.dark {\n  }\n`;\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (darkTheme);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi90aGVtZS9kYXJrVGhlbWUudHMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQXVDO0FBRXZDLE1BQU1DLFlBQVlELGtEQUFHLENBQUM7OztBQUd0QixDQUFDO0FBRUQsaUVBQWVDLFNBQVNBLEVBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi90aGVtZS9kYXJrVGhlbWUudHM/MGNjOCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjc3MgfSBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIlxuXG5jb25zdCBkYXJrVGhlbWUgPSBjc3NgXG4gIGJvZHkuZGFyayB7XG4gIH1cbmBcblxuZXhwb3J0IGRlZmF1bHQgZGFya1RoZW1lXG4iXSwibmFtZXMiOlsiY3NzIiwiZGFya1RoZW1lIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./theme/darkTheme.ts\n");

/***/ }),

/***/ "./theme/defaultTheme.ts":
/*!*******************************!*\
  !*** ./theme/defaultTheme.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _themeConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./themeConfig */ \"./theme/themeConfig.ts\");\n\n\nconst camelToSnakeCase = (str)=>str.replace(/[A-Z]/g, (letter)=>`-${letter.toLowerCase()}`);\nconst mapTheme = (variables)=>{\n    return Object.keys(variables).reduce((acc, key)=>{\n        return {\n            ...acc,\n            [`--${camelToSnakeCase(key)}`]: variables[key]\n        };\n    }, {});\n};\nconst colors = mapTheme(_themeConfig__WEBPACK_IMPORTED_MODULE_1__.COLORS);\nconst defaultTheme = styled_components__WEBPACK_IMPORTED_MODULE_0__.css`\n  :root {\n    ${()=>styled_components__WEBPACK_IMPORTED_MODULE_0__.css`\n      ${Object.keys(colors).map((property)=>`${property}:${colors[property]};`)}\n    `}\n  }\n`;\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (defaultTheme);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi90aGVtZS9kZWZhdWx0VGhlbWUudHMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUF1QztBQUNEO0FBS3RDLE1BQU1FLG1CQUFtQixDQUFDQyxNQUFnQkEsSUFBSUMsT0FBTyxDQUFDLFVBQVUsQ0FBQ0MsU0FBVyxDQUFDLENBQUMsRUFBRUEsT0FBT0MsV0FBVyxHQUFHLENBQUM7QUFDdEcsTUFBTUMsV0FBVyxDQUFDQyxZQUFvRDtJQUNwRSxPQUFPQyxPQUFPQyxJQUFJLENBQUNGLFdBQVdHLE1BQU0sQ0FBQyxDQUFDQyxLQUFLQyxNQUFRO1FBQ2pELE9BQU87WUFBRSxHQUFHRCxHQUFHO1lBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRVYsaUJBQWlCVyxLQUFLLENBQUMsQ0FBQyxFQUFFTCxTQUFTLENBQUNLLElBQUk7UUFBQztJQUNsRSxHQUFHLENBQUM7QUFDTjtBQUVBLE1BQU1DLFNBQVNQLFNBQVNOLGdEQUFNQTtBQUM5QixNQUFNYyxlQUFlZixrREFBRyxDQUFDOztJQUVyQixFQUFFLElBQU1BLGtEQUFHLENBQUM7TUFDVixFQUFFUyxPQUFPQyxJQUFJLENBQUNJLFFBQVFFLEdBQUcsQ0FBQyxDQUFDQyxXQUFhLENBQUMsRUFBRUEsU0FBUyxDQUFDLEVBQUVILE1BQU0sQ0FBQ0csU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFO0lBQzlFLENBQUMsQ0FBQzs7QUFFTixDQUFDO0FBRUQsaUVBQWVGLFlBQVlBLEVBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi90aGVtZS9kZWZhdWx0VGhlbWUudHM/NWY5MyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjc3MgfSBmcm9tIFwic3R5bGVkLWNvbXBvbmVudHNcIlxuaW1wb3J0IHsgQ09MT1JTIH0gZnJvbSBcIi4vdGhlbWVDb25maWdcIlxuaW50ZXJmYWNlIElNYXBwZWRUaGVtZSB7XG4gIFtrZXk6IHN0cmluZ106IHN0cmluZyB8IG51bGxcbn1cblxuY29uc3QgY2FtZWxUb1NuYWtlQ2FzZSA9IChzdHI6IHN0cmluZykgPT4gc3RyLnJlcGxhY2UoL1tBLVpdL2csIChsZXR0ZXIpID0+IGAtJHtsZXR0ZXIudG9Mb3dlckNhc2UoKX1gKVxuY29uc3QgbWFwVGhlbWUgPSAodmFyaWFibGVzOiBSZWNvcmQ8c3RyaW5nLCBzdHJpbmc+KTogSU1hcHBlZFRoZW1lID0+IHtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKHZhcmlhYmxlcykucmVkdWNlKChhY2MsIGtleSkgPT4ge1xuICAgIHJldHVybiB7IC4uLmFjYywgW2AtLSR7Y2FtZWxUb1NuYWtlQ2FzZShrZXkpfWBdOiB2YXJpYWJsZXNba2V5XSB9XG4gIH0sIHt9KVxufVxuXG5jb25zdCBjb2xvcnMgPSBtYXBUaGVtZShDT0xPUlMpXG5jb25zdCBkZWZhdWx0VGhlbWUgPSBjc3NgXG4gIDpyb290IHtcbiAgICAkeygpID0+IGNzc2BcbiAgICAgICR7T2JqZWN0LmtleXMoY29sb3JzKS5tYXAoKHByb3BlcnR5KSA9PiBgJHtwcm9wZXJ0eX06JHtjb2xvcnNbcHJvcGVydHldfTtgKX1cbiAgICBgfVxuICB9XG5gXG5cbmV4cG9ydCBkZWZhdWx0IGRlZmF1bHRUaGVtZVxuIl0sIm5hbWVzIjpbImNzcyIsIkNPTE9SUyIsImNhbWVsVG9TbmFrZUNhc2UiLCJzdHIiLCJyZXBsYWNlIiwibGV0dGVyIiwidG9Mb3dlckNhc2UiLCJtYXBUaGVtZSIsInZhcmlhYmxlcyIsIk9iamVjdCIsImtleXMiLCJyZWR1Y2UiLCJhY2MiLCJrZXkiLCJjb2xvcnMiLCJkZWZhdWx0VGhlbWUiLCJtYXAiLCJwcm9wZXJ0eSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./theme/defaultTheme.ts\n");

/***/ }),

/***/ "./theme/index.tsx":
/*!*************************!*\
  !*** ./theme/index.tsx ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"GlobalStyle\": () => (/* binding */ GlobalStyle)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _darkTheme__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./darkTheme */ \"./theme/darkTheme.ts\");\n/* harmony import */ var _defaultTheme__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./defaultTheme */ \"./theme/defaultTheme.ts\");\n/* harmony import */ var _lightTheme__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lightTheme */ \"./theme/lightTheme.ts\");\n\n\n\n\nconst GlobalStyle = styled_components__WEBPACK_IMPORTED_MODULE_0__.createGlobalStyle`\n  ${_defaultTheme__WEBPACK_IMPORTED_MODULE_2__[\"default\"]}\n  ${_lightTheme__WEBPACK_IMPORTED_MODULE_3__[\"default\"]}\n  ${_darkTheme__WEBPACK_IMPORTED_MODULE_1__[\"default\"]}\n  body {\n    overflow-x: auto;\n  }\n  * {\n    font-family: Spartan,FC Minimal;\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n  }\n  div,textarea,a,span {\n    color: var(--color-text-primary);\n\n  }\n  textarea {\n    font-family: Spartan,FC Minimal !important;\n  }\n  a {\n    text-decoration:none;\n  }\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi90aGVtZS9pbmRleC50c3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQXNEO0FBQ2xCO0FBQ007QUFDSjtBQUUvQixNQUFNSSxjQUFjSixnRUFBaUIsQ0FBQztFQUMzQyxFQUFFRSxxREFBWUEsQ0FBQztFQUNmLEVBQUVDLG1EQUFVQSxDQUFDO0VBQ2IsRUFBRUYsa0RBQVNBLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQmQsQ0FBQyxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vdGhlbWUvaW5kZXgudHN4PzIzYWMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY3JlYXRlR2xvYmFsU3R5bGUgfSBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5pbXBvcnQgZGFya1RoZW1lIGZyb20gJy4vZGFya1RoZW1lJztcbmltcG9ydCBkZWZhdWx0VGhlbWUgZnJvbSAnLi9kZWZhdWx0VGhlbWUnO1xuaW1wb3J0IGxpZ2h0VGhlbWUgZnJvbSAnLi9saWdodFRoZW1lJztcblxuZXhwb3J0IGNvbnN0IEdsb2JhbFN0eWxlID0gY3JlYXRlR2xvYmFsU3R5bGVgXG4gICR7ZGVmYXVsdFRoZW1lfVxuICAke2xpZ2h0VGhlbWV9XG4gICR7ZGFya1RoZW1lfVxuICBib2R5IHtcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xuICB9XG4gICoge1xuICAgIGZvbnQtZmFtaWx5OiBTcGFydGFuLEZDIE1pbmltYWw7XG4gICAgLXdlYmtpdC1mb250LXNtb290aGluZzogYW50aWFsaWFzZWQ7XG4gICAgLW1vei1vc3gtZm9udC1zbW9vdGhpbmc6IGdyYXlzY2FsZTtcbiAgfVxuICBkaXYsdGV4dGFyZWEsYSxzcGFuIHtcbiAgICBjb2xvcjogdmFyKC0tY29sb3ItdGV4dC1wcmltYXJ5KTtcblxuICB9XG4gIHRleHRhcmVhIHtcbiAgICBmb250LWZhbWlseTogU3BhcnRhbixGQyBNaW5pbWFsICFpbXBvcnRhbnQ7XG4gIH1cbiAgYSB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOm5vbmU7XG4gIH1cbmA7XG4iXSwibmFtZXMiOlsiY3JlYXRlR2xvYmFsU3R5bGUiLCJkYXJrVGhlbWUiLCJkZWZhdWx0VGhlbWUiLCJsaWdodFRoZW1lIiwiR2xvYmFsU3R5bGUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./theme/index.tsx\n");

/***/ }),

/***/ "./theme/lightTheme.ts":
/*!*****************************!*\
  !*** ./theme/lightTheme.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);\n\nconst lightTheme = styled_components__WEBPACK_IMPORTED_MODULE_0__.css`\n  :root {\n  }\n`;\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (lightTheme);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi90aGVtZS9saWdodFRoZW1lLnRzLmpzIiwibWFwcGluZ3MiOiI7Ozs7OztBQUF1QztBQUV2QyxNQUFNQyxhQUFhRCxrREFBRyxDQUFDOzs7QUFHdkIsQ0FBQztBQUVELGlFQUFlQyxVQUFVQSxFQUFBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vdGhlbWUvbGlnaHRUaGVtZS50cz9iMGRmIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNzcyB9IGZyb20gXCJzdHlsZWQtY29tcG9uZW50c1wiXG5cbmNvbnN0IGxpZ2h0VGhlbWUgPSBjc3NgXG4gIDpyb290IHtcbiAgfVxuYFxuXG5leHBvcnQgZGVmYXVsdCBsaWdodFRoZW1lXG4iXSwibmFtZXMiOlsiY3NzIiwibGlnaHRUaGVtZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./theme/lightTheme.ts\n");

/***/ }),

/***/ "./theme/themeConfig.ts":
/*!******************************!*\
  !*** ./theme/themeConfig.ts ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"COLORS\": () => (/* binding */ COLORS),\n/* harmony export */   \"ThemeConfig\": () => (/* binding */ ThemeConfig)\n/* harmony export */ });\nconst COLORS = {\n    colorPrimary: \"#DF0075\",\n    colorGradient: \"linear-gradient(276.42deg, #E81D75 0%, #5B108B 100%);\",\n    colorTextPrimary: \"#000\",\n    colorTextTertiary: \"#6F767E\",\n    colorWhite: \"#fff\",\n    colorShadow1: \"rgba(0, 0, 0, 0.1)\",\n    colorShadow2: \"rgba(0, 0, 0, 0.06)\",\n    colorShadow3: \"rgba(0, 0, 0, 0.04)\",\n    colorSurfaceSecondary: \"#EFF0F6\",\n    colorSurfaceTertiary: \"#DEE0ED\",\n    colorLink: \"#5a6189\",\n    colorBorder: \"#d9d9d9\",\n    colorText1: \"#32325d\",\n    colorText2: \"#fff\",\n    colorBgPrimary: \"#F6F7FC\",\n    colorRank1: \"#dd8d19\",\n    colorRank2: \"#ADAB9F\",\n    colorRank3: \"#986350\"\n};\nconst ThemeConfig = {\n    font: {\n        h1: \"40px\",\n        h1Mobile: \"32px\",\n        h2: \"32px\",\n        h2Mobile: \"24px\",\n        h3: \"24px\",\n        h3Mobile: \"20px\",\n        h4: \"20px\",\n        title: \"15px\",\n        titleMobile: \"12px\",\n        paragraph1: \"18px\",\n        paragraph1Mobile: \"12px\",\n        paragraph2: \"14px\",\n        textLink: \"14px\",\n        placeholder: \"12px\",\n        tag: \"11px\"\n    },\n    screen: {\n        desktop_xl: \"1980px\",\n        desktop_x: \"1280px\",\n        desktop: \"1024px\",\n        mobile: \"575px\",\n        tablet: \"820px\"\n    },\n    colors: COLORS\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi90aGVtZS90aGVtZUNvbmZpZy50cy5qcyIsIm1hcHBpbmdzIjoiOzs7OztBQUFPLE1BQU1BLFNBQVM7SUFDcEJDLGNBQWM7SUFDZEMsZUFBZTtJQUNmQyxrQkFBa0I7SUFDbEJDLG1CQUFtQjtJQUNuQkMsWUFBWTtJQUNaQyxjQUFjO0lBQ2RDLGNBQWM7SUFDZEMsY0FBYztJQUNkQyx1QkFBdUI7SUFDdkJDLHNCQUFzQjtJQUN0QkMsV0FBVztJQUNYQyxhQUFhO0lBQ2JDLFlBQVk7SUFDWkMsWUFBWTtJQUNaQyxnQkFBZ0I7SUFDaEJDLFlBQVk7SUFDWkMsWUFBWTtJQUNaQyxZQUFZO0FBQ2QsRUFBQztBQUNNLE1BQU1DLGNBQWM7SUFDekJDLE1BQU07UUFDSkMsSUFBSTtRQUNKQyxVQUFVO1FBQ1ZDLElBQUk7UUFDSkMsVUFBVTtRQUNWQyxJQUFJO1FBQ0pDLFVBQVU7UUFDVkMsSUFBSTtRQUNKQyxPQUFPO1FBQ1BDLGFBQWE7UUFDYkMsWUFBWTtRQUNaQyxrQkFBa0I7UUFDbEJDLFlBQVk7UUFDWkMsVUFBVTtRQUNWQyxhQUFhO1FBQ2JDLEtBQUs7SUFDUDtJQUNBQyxRQUFRO1FBQ05DLFlBQVk7UUFDWkMsV0FBVztRQUNYQyxTQUFTO1FBQ1RDLFFBQVE7UUFDUkMsUUFBUTtJQUNWO0lBQ0FDLFFBQVExQztBQUNWLEVBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi90aGVtZS90aGVtZUNvbmZpZy50cz85MGRhIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBDT0xPUlMgPSB7XG4gIGNvbG9yUHJpbWFyeTogXCIjREYwMDc1XCIsXG4gIGNvbG9yR3JhZGllbnQ6IFwibGluZWFyLWdyYWRpZW50KDI3Ni40MmRlZywgI0U4MUQ3NSAwJSwgIzVCMTA4QiAxMDAlKTtcIixcbiAgY29sb3JUZXh0UHJpbWFyeTogXCIjMDAwXCIsXG4gIGNvbG9yVGV4dFRlcnRpYXJ5OiBcIiM2Rjc2N0VcIixcbiAgY29sb3JXaGl0ZTogXCIjZmZmXCIsXG4gIGNvbG9yU2hhZG93MTogXCJyZ2JhKDAsIDAsIDAsIDAuMSlcIixcbiAgY29sb3JTaGFkb3cyOiBcInJnYmEoMCwgMCwgMCwgMC4wNilcIixcbiAgY29sb3JTaGFkb3czOiBcInJnYmEoMCwgMCwgMCwgMC4wNClcIixcbiAgY29sb3JTdXJmYWNlU2Vjb25kYXJ5OiBcIiNFRkYwRjZcIixcbiAgY29sb3JTdXJmYWNlVGVydGlhcnk6IFwiI0RFRTBFRFwiLFxuICBjb2xvckxpbms6IFwiIzVhNjE4OVwiLFxuICBjb2xvckJvcmRlcjogXCIjZDlkOWQ5XCIsXG4gIGNvbG9yVGV4dDE6IFwiIzMyMzI1ZFwiLFxuICBjb2xvclRleHQyOiBcIiNmZmZcIixcbiAgY29sb3JCZ1ByaW1hcnk6IFwiI0Y2RjdGQ1wiLFxuICBjb2xvclJhbmsxOiBcIiNkZDhkMTlcIixcbiAgY29sb3JSYW5rMjogXCIjQURBQjlGXCIsXG4gIGNvbG9yUmFuazM6IFwiIzk4NjM1MFwiLFxufVxuZXhwb3J0IGNvbnN0IFRoZW1lQ29uZmlnID0ge1xuICBmb250OiB7XG4gICAgaDE6IFwiNDBweFwiLFxuICAgIGgxTW9iaWxlOiBcIjMycHhcIixcbiAgICBoMjogXCIzMnB4XCIsXG4gICAgaDJNb2JpbGU6IFwiMjRweFwiLFxuICAgIGgzOiBcIjI0cHhcIixcbiAgICBoM01vYmlsZTogXCIyMHB4XCIsXG4gICAgaDQ6IFwiMjBweFwiLFxuICAgIHRpdGxlOiBcIjE1cHhcIixcbiAgICB0aXRsZU1vYmlsZTogXCIxMnB4XCIsXG4gICAgcGFyYWdyYXBoMTogXCIxOHB4XCIsXG4gICAgcGFyYWdyYXBoMU1vYmlsZTogXCIxMnB4XCIsXG4gICAgcGFyYWdyYXBoMjogXCIxNHB4XCIsXG4gICAgdGV4dExpbms6IFwiMTRweFwiLFxuICAgIHBsYWNlaG9sZGVyOiBcIjEycHhcIixcbiAgICB0YWc6IFwiMTFweFwiLFxuICB9LFxuICBzY3JlZW46IHtcbiAgICBkZXNrdG9wX3hsOiBcIjE5ODBweFwiLFxuICAgIGRlc2t0b3BfeDogXCIxMjgwcHhcIixcbiAgICBkZXNrdG9wOiBcIjEwMjRweFwiLFxuICAgIG1vYmlsZTogXCI1NzVweFwiLFxuICAgIHRhYmxldDogXCI4MjBweFwiLFxuICB9LFxuICBjb2xvcnM6IENPTE9SUyxcbn1cbiJdLCJuYW1lcyI6WyJDT0xPUlMiLCJjb2xvclByaW1hcnkiLCJjb2xvckdyYWRpZW50IiwiY29sb3JUZXh0UHJpbWFyeSIsImNvbG9yVGV4dFRlcnRpYXJ5IiwiY29sb3JXaGl0ZSIsImNvbG9yU2hhZG93MSIsImNvbG9yU2hhZG93MiIsImNvbG9yU2hhZG93MyIsImNvbG9yU3VyZmFjZVNlY29uZGFyeSIsImNvbG9yU3VyZmFjZVRlcnRpYXJ5IiwiY29sb3JMaW5rIiwiY29sb3JCb3JkZXIiLCJjb2xvclRleHQxIiwiY29sb3JUZXh0MiIsImNvbG9yQmdQcmltYXJ5IiwiY29sb3JSYW5rMSIsImNvbG9yUmFuazIiLCJjb2xvclJhbmszIiwiVGhlbWVDb25maWciLCJmb250IiwiaDEiLCJoMU1vYmlsZSIsImgyIiwiaDJNb2JpbGUiLCJoMyIsImgzTW9iaWxlIiwiaDQiLCJ0aXRsZSIsInRpdGxlTW9iaWxlIiwicGFyYWdyYXBoMSIsInBhcmFncmFwaDFNb2JpbGUiLCJwYXJhZ3JhcGgyIiwidGV4dExpbmsiLCJwbGFjZWhvbGRlciIsInRhZyIsInNjcmVlbiIsImRlc2t0b3BfeGwiLCJkZXNrdG9wX3giLCJkZXNrdG9wIiwibW9iaWxlIiwidGFibGV0IiwiY29sb3JzIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./theme/themeConfig.ts\n");

/***/ }),

/***/ "./pages/styles.css":
/*!**************************!*\
  !*** ./pages/styles.css ***!
  \**************************/
/***/ (() => {



/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.tsx"));
module.exports = __webpack_exports__;

})();