"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./containers/Home/Components/HomeBanner.tsx":
/*!***************************************************!*\
  !*** ./containers/Home/Components/HomeBanner.tsx ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);\n\n\nconst HomeBanner = ()=>{\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Banner, {}, void 0, false, {\n            fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/containers/Home/Components/HomeBanner.tsx\",\n            lineNumber: 6,\n            columnNumber: 7\n        }, undefined)\n    }, void 0, false);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HomeBanner);\nconst Banner = (styled_components__WEBPACK_IMPORTED_MODULE_1___default().div)`\n  /* max-width: 640px;\n  max-height: 140px; */\n  width: 640px;\n  height: 140px;\n  background-image: url('/statics/images/avatar-logo.png');\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb250YWluZXJzL0hvbWUvQ29tcG9uZW50cy9Ib21lQmFubmVyLnRzeC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQXVDO0FBRXZDLE1BQU1DLGFBQWEsSUFBTTtJQUN2QixxQkFDRTtrQkFDRSw0RUFBQ0M7Ozs7OztBQUdQO0FBQ0EsaUVBQWVELFVBQVVBLEVBQUM7QUFFMUIsTUFBTUMsU0FBU0YsOERBQVUsQ0FBQzs7Ozs7O0FBTTFCLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9jb250YWluZXJzL0hvbWUvQ29tcG9uZW50cy9Ib21lQmFubmVyLnRzeD9kNTZlIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBIb21lQmFubmVyID0gKCkgPT4ge1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8QmFubmVyIC8+XG4gICAgPC8+XG4gICk7XG59O1xuZXhwb3J0IGRlZmF1bHQgSG9tZUJhbm5lcjtcblxuY29uc3QgQmFubmVyID0gc3R5bGVkLmRpdmBcbiAgLyogbWF4LXdpZHRoOiA2NDBweDtcbiAgbWF4LWhlaWdodDogMTQwcHg7ICovXG4gIHdpZHRoOiA2NDBweDtcbiAgaGVpZ2h0OiAxNDBweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvc3RhdGljcy9pbWFnZXMvYXZhdGFyLWxvZ28ucG5nJyk7XG5gO1xuIl0sIm5hbWVzIjpbInN0eWxlZCIsIkhvbWVCYW5uZXIiLCJCYW5uZXIiLCJkaXYiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./containers/Home/Components/HomeBanner.tsx\n");

/***/ }),

/***/ "./containers/Home/index.tsx":
/*!***********************************!*\
  !*** ./containers/Home/index.tsx ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _Components_HomeBanner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Components/HomeBanner */ \"./containers/Home/Components/HomeBanner.tsx\");\n\n\n\n\nconst HomePage = ()=>{\n    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();\n    const handleOnClick = ()=>{\n        router.push(\"/select-avatar\");\n    };\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Container, {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_Components_HomeBanner__WEBPACK_IMPORTED_MODULE_3__[\"default\"], {}, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/containers/Home/index.tsx\",\n                lineNumber: 12,\n                columnNumber: 7\n            }, undefined),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Button, {\n                onClick: handleOnClick,\n                children: \"Designer Mode\"\n            }, void 0, false, {\n                fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/containers/Home/index.tsx\",\n                lineNumber: 13,\n                columnNumber: 7\n            }, undefined)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/containers/Home/index.tsx\",\n        lineNumber: 11,\n        columnNumber: 5\n    }, undefined);\n};\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (HomePage);\nconst Container = (styled_components__WEBPACK_IMPORTED_MODULE_2___default().div)`\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n  height: 100vh;\n  flex-direction: column;\n`;\nconst Button = (styled_components__WEBPACK_IMPORTED_MODULE_2___default().div)`\n  margin-top: 160px;\n  width: 380px;\n  height: 56px;\n  background-image: url('/statics/images/frame-button-pink.png');\n  background-repeat: no-repeat;\n  background-size: 100% 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  color: #fff;\n  cursor: pointer;\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb250YWluZXJzL0hvbWUvaW5kZXgudHN4LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7QUFBd0M7QUFDRDtBQUNVO0FBRWpELE1BQU1HLFdBQVcsSUFBTTtJQUNyQixNQUFNQyxTQUFTSixzREFBU0E7SUFDeEIsTUFBTUssZ0JBQWdCLElBQU07UUFDMUJELE9BQU9FLElBQUksQ0FBQztJQUNkO0lBQ0EscUJBQ0UsOERBQUNDOzswQkFDQyw4REFBQ0wsOERBQVVBOzs7OzswQkFDWCw4REFBQ007Z0JBQU9DLFNBQVNKOzBCQUFlOzs7Ozs7Ozs7Ozs7QUFHdEM7QUFDQSxpRUFBZUYsUUFBUUEsRUFBQztBQUV4QixNQUFNSSxZQUFZTiw4REFBVSxDQUFDOzs7Ozs7O0FBTzdCLENBQUM7QUFFRCxNQUFNTyxTQUFTUCw4REFBVSxDQUFDOzs7Ozs7Ozs7Ozs7QUFZMUIsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2NvbnRhaW5lcnMvSG9tZS9pbmRleC50c3g/NzFmOCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcic7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcbmltcG9ydCBIb21lQmFubmVyIGZyb20gJy4vQ29tcG9uZW50cy9Ib21lQmFubmVyJztcblxuY29uc3QgSG9tZVBhZ2UgPSAoKSA9PiB7XG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xuICBjb25zdCBoYW5kbGVPbkNsaWNrID0gKCkgPT4ge1xuICAgIHJvdXRlci5wdXNoKCcvc2VsZWN0LWF2YXRhcicpO1xuICB9O1xuICByZXR1cm4gKFxuICAgIDxDb250YWluZXI+XG4gICAgICA8SG9tZUJhbm5lciAvPlxuICAgICAgPEJ1dHRvbiBvbkNsaWNrPXtoYW5kbGVPbkNsaWNrfT5EZXNpZ25lciBNb2RlPC9CdXR0b24+XG4gICAgPC9Db250YWluZXI+XG4gICk7XG59O1xuZXhwb3J0IGRlZmF1bHQgSG9tZVBhZ2U7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbmA7XG5cbmNvbnN0IEJ1dHRvbiA9IHN0eWxlZC5kaXZgXG4gIG1hcmdpbi10b3A6IDE2MHB4O1xuICB3aWR0aDogMzgwcHg7XG4gIGhlaWdodDogNTZweDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvc3RhdGljcy9pbWFnZXMvZnJhbWUtYnV0dG9uLXBpbmsucG5nJyk7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJSAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgY29sb3I6ICNmZmY7XG4gIGN1cnNvcjogcG9pbnRlcjtcbmA7XG4iXSwibmFtZXMiOlsidXNlUm91dGVyIiwic3R5bGVkIiwiSG9tZUJhbm5lciIsIkhvbWVQYWdlIiwicm91dGVyIiwiaGFuZGxlT25DbGljayIsInB1c2giLCJDb250YWluZXIiLCJCdXR0b24iLCJvbkNsaWNrIiwiZGl2Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./containers/Home/index.tsx\n");

/***/ }),

/***/ "./pages/index.tsx":
/*!*************************!*\
  !*** ./pages/index.tsx ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Index\": () => (/* binding */ Index),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var containers_Home__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! containers/Home */ \"./containers/Home/index.tsx\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ \"styled-components\");\n/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nfunction Index() {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Container, {\n        children: /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(containers_Home__WEBPACK_IMPORTED_MODULE_1__[\"default\"], {}, void 0, false, {\n            fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/index.tsx\",\n            lineNumber: 7,\n            columnNumber: 7\n        }, this)\n    }, void 0, false, {\n        fileName: \"/Users/joe/Documents/WJAMES-Project/Game/avatar/apps/frontend/pages/index.tsx\",\n        lineNumber: 6,\n        columnNumber: 5\n    }, this);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Index);\nconst Container = (styled_components__WEBPACK_IMPORTED_MODULE_2___default().div)`\n  position: relative;\n`;\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC50c3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBO0FBQXVDO0FBQ0E7QUFFaEMsU0FBU0UsUUFBUTtJQUN0QixxQkFDRSw4REFBQ0M7a0JBQ0MsNEVBQUNILHVEQUFRQTs7Ozs7Ozs7OztBQUdmLENBQUM7QUFFRCxpRUFBZUUsS0FBS0EsRUFBQztBQUVyQixNQUFNQyxZQUFZRiw4REFBVSxDQUFDOztBQUU3QixDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXgudHN4PzA3ZmYiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEhvbWVQYWdlIGZyb20gJ2NvbnRhaW5lcnMvSG9tZSc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuZXhwb3J0IGZ1bmN0aW9uIEluZGV4KCkge1xuICByZXR1cm4gKFxuICAgIDxDb250YWluZXI+XG4gICAgICA8SG9tZVBhZ2UgLz5cbiAgICA8L0NvbnRhaW5lcj5cbiAgKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgSW5kZXg7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbmA7XG4iXSwibmFtZXMiOlsiSG9tZVBhZ2UiLCJzdHlsZWQiLCJJbmRleCIsIkNvbnRhaW5lciIsImRpdiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/index.tsx\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.tsx"));
module.exports = __webpack_exports__;

})();