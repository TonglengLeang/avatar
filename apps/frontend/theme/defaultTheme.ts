import { css } from "styled-components"
import { COLORS } from "./themeConfig"
interface IMappedTheme {
  [key: string]: string | null
}

const camelToSnakeCase = (str: string) => str.replace(/[A-Z]/g, (letter) => `-${letter.toLowerCase()}`)
const mapTheme = (variables: Record<string, string>): IMappedTheme => {
  return Object.keys(variables).reduce((acc, key) => {
    return { ...acc, [`--${camelToSnakeCase(key)}`]: variables[key] }
  }, {})
}

const colors = mapTheme(COLORS)
const defaultTheme = css`
  :root {
    ${() => css`
      ${Object.keys(colors).map((property) => `${property}:${colors[property]};`)}
    `}
  }
`

export default defaultTheme
