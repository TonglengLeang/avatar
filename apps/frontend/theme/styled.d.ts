// styled.d.ts
import "styled-components"

declare module "styled-components" {
  export interface DefaultTheme {
    font: {
      /**
       * @remark 40px;
       */
      h1: string
      /**
       * @remark 32px;
       */
      h1Mobile: string
      /**
       * @remark 32px;
       */
      h2: string
      /**
       * @remark 24px;
       */
      h2Mobile: string
      /**
       * @remark 24px;
       */
      h3: string
      /**
       * @remark 24px;
       */
      h3Mobile: string
      /**
       * @remark 20px;
       */
      h4: string
      /**
       * @remark 15px;
       */
      title: string
      /**
       * @remark 12px;
       */
      titleMobile: string
      /**
       * @remark 18px;
       */
      paragraph1: string
      /**
       * @remark 12px;
       */
      paragraph1Mobile: string
      /**
       * @remark 14px;
       */
      paragraph2: string
      /**
       * @remark 14px;
       */
      textLink: string
      /**
       * @remark 12px;
       */
      placeholder: string
      /**
       * @remark 11px;
       */
      tag: string
    }
    screen: {
      desktop_x: string
      desktop: string
      mobile: string
      tablet: string
    }
    colors: {
      [key: string]: string
    }
  }
}
