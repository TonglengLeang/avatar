import { createGlobalStyle } from 'styled-components';
import darkTheme from './darkTheme';
import defaultTheme from './defaultTheme';
import lightTheme from './lightTheme';

export const GlobalStyle = createGlobalStyle`
  ${defaultTheme}
  ${lightTheme}
  ${darkTheme}
  body {
    overflow-x: auto;
  }
  * {
    font-family: Spartan,FC Minimal;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  div,textarea,a,span {
    color: var(--color-text-primary);

  }
  textarea {
    font-family: Spartan,FC Minimal !important;
  }
  a {
    text-decoration:none;
  }
`;
