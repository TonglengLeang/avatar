import styled from 'styled-components';

const HomeBanner = () => {
  return (
    <>
      <Banner />
    </>
  );
};
export default HomeBanner;

const Banner = styled.div`
  /* max-width: 640px;
  max-height: 140px; */
  width: 640px;
  height: 140px;
  background-image: url('/statics/images/avatar-logo.png');
`;
