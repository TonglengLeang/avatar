import { useRouter } from 'next/router';
import styled from 'styled-components';
import HomeBanner from './Components/HomeBanner';

const HomePage = () => {
  const router = useRouter();
  const handleOnClick = () => {
    router.push('/select-avatar');
  };
  return (
    <Container>
      <HomeBanner />
      <Button onClick={handleOnClick}>Designer Mode</Button>
    </Container>
  );
};
export default HomePage;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  flex-direction: column;
`;

const Button = styled.div`
  margin-top: 160px;
  width: 380px;
  height: 56px;
  background-image: url('/statics/images/frame-button-pink.png');
  background-repeat: no-repeat;
  background-size: 100% 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  cursor: pointer;
`;
