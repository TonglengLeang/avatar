import Navbar from 'components/Header/Navbar';
import SwipperCard from 'components/SwiperCard';
import styled from 'styled-components';

const SelectAvatar = () => {
  return (
    <Container>
      <Navbar />
      <SwipperCard />
    </Container>
  );
};
export default SelectAvatar;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  flex-direction: column;
`;
