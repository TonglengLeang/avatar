import styled from 'styled-components';

const Avatar = () => {
  return (
    <Container>
      <AvatarImage></AvatarImage>
    </Container>
  );
};
export default Avatar;

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const AvatarImage = styled.div`
  width: 80%;
  height: 95vh;
  position: fixed;
  bottom: 0;
  background-image: url('/statics/images/mid-avatar.png');
  background-size: 100% 100%;
`;
