import { LayouWrapper } from 'Layout';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from 'theme';
import { ThemeConfig } from 'theme/themeConfig';
import './styles.css';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={ThemeConfig}>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
      </Head>
      <GlobalStyle />
      <LayouWrapper>
        <Component {...pageProps} />
      </LayouWrapper>
    </ThemeProvider>
  );
}

export default CustomApp;
