import HomePage from 'containers/Home';
import styled from 'styled-components';

export function Index() {
  return (
    <Container>
      <HomePage />
    </Container>
  );
}

export default Index;

const Container = styled.div`
  position: relative;
`;
