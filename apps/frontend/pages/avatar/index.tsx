import Navbar from 'components/Header/Navbar';
import AvatarPage from 'containers/Avatar';
import styled from 'styled-components';

const Avatar = () => {
  return (
    <Container>
      <Navbar />
      <AvatarPage />
    </Container>
  );
};

export default Avatar;

const Container = styled.div`
  position: relative;
`;
