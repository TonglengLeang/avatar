import styled from 'styled-components';
import SelectAvatarPage from 'containers/SelectAvatar';
const SelectAvatar = () => {
  return (
    <Container>
      <SelectAvatarPage />
    </Container>
  );
};

export default SelectAvatar;

const Container = styled.div`
  position: relative;
`;
