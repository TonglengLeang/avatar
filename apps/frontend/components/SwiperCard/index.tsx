import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import { EffectCoverflow, Pagination, Navigation } from 'swiper';

import styled from 'styled-components';
import LazyImage from 'components/Image/LazyImage';

const SwipperCard = () => {
  const images = [
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
    { imageTitle: 'Avatar 1', imageUrl: '/statics/images/avatar-human.png' },
  ];

  return (
    <Container>
      <div className="container">
        <Swiper
          effect={'coverflow'}
          grabCursor={true}
          centeredSlides={true}
          loop={true}
          slidesPerView={3}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 2.5,
          }}
          pagination={{ el: '.swiper-pagination', clickable: true }}
          navigation={{
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
            // clickable: true,
          }}
          modules={[EffectCoverflow, Pagination, Navigation]}
          className="swiper_container"
        >
          {images.map((image, index) => {
            return (
              <CustomSwiperSlide key={index}>
                <div>
                  <LazyImage
                    width={854}
                    height={222}
                    src={image.imageUrl}
                    alt={image.imageTitle}
                  />
                  {/* <img src={image.imageUrl} alt="slide_image" /> */}
                </div>
              </CustomSwiperSlide>
            );
          })}
        </Swiper>
        {/* <div className="slider-controler"> */}
        {/* <div className="swiper-button-prev slider-arrow">{'<'}</div>
          <div className="swiper-button-next slider-arrow">{'>'}</div> */}
        {/* <div className="swiper-pagination"></div> */}
        {/* </div> */}
      </div>
    </Container>
  );
};
export default SwipperCard;

const Container = styled.div`
  ::-webkit-scrollbar {
    width: 1.3rem;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 1rem;
    background: #797979;
    transition: all 0.5s ease-in-out;
  }

  ::-webkit-scrollbar-thumb:hover {
    background: #222224;
  }

  ::-webkit-scrollbar-track {
    background: #f9f9f9;
  }

  .container {
    max-width: 124rem;
    padding: 4rem 1rem;
    margin: 0 auto;
    margin-top: 10rem;
  }

  .heading {
    padding: 1rem 0;
    font-size: 3.5rem;
    text-align: center;
  }

  .swiper_container {
    height: 52rem;
    padding: 2rem 0;
    position: relative;
  }

  .swiper-slide {
    width: 37rem;
    height: 42rem;
    position: relative;
  }

  @media (max-width: 500px) {
    .swiper_container {
      height: 47rem;
    }
    .swiper-slide {
      width: 28rem !important;
      height: 36rem !important;
    }
    .swiper-slide img {
      width: 28rem !important;
      height: 36rem !important;
    }
  }

  .swiper-slide img {
    width: 37rem;
    height: 42rem;
    border-radius: 2rem;
    object-fit: cover;
  }

  .swiper-slide-shadow-left,
  .swiper-slide-shadow-right {
    display: none;
  }

  .slider-controler {
    position: relative;
    bottom: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .slider-controler .swiper-button-next {
    left: 58% !important;
    transform: translateX(-58%) !important;
  }

  @media (max-width: 990px) {
    .slider-controler .swiper-button-next {
      left: 70% !important;
      transform: translateX(-70%) !important;
    }
  }

  @media (max-width: 450px) {
    .slider-controler .swiper-button-next {
      left: 80% !important;
      transform: translateX(-80%) !important;
    }
  }

  @media (max-width: 990px) {
    .slider-controler .swiper-button-prev {
      left: 30% !important;
      transform: translateX(-30%) !important;
    }
  }

  @media (max-width: 450px) {
    .slider-controler .swiper-button-prev {
      left: 20% !important;
      transform: translateX(-20%) !important;
    }
  }

  .slider-controler .slider-arrow {
    background: var(--white);
    width: 3.5rem;
    height: 3.5rem;
    border-radius: 50%;
    left: 42%;
    transform: translateX(-42%);
    filter: drop-shadow(0px 8px 24px rgba(18, 28, 53, 0.1));
  }

  .slider-controler .slider-arrow ion-icon {
    font-size: 2rem;
    color: #2222 24;
  }

  .slider-controler .slider-arrow::after {
    content: '';
  }

  .swiper-pagination {
    position: relative;
    width: 15rem !important;
    bottom: 1rem;
  }

  .swiper-pagination .swiper-pagination-bullet {
    filter: drop-shadow(0px 8px 24px rgba(18, 28, 53, 0.1));
  }

  .swiper-pagination .swiper-pagination-bullet-active {
    background: var(--primary);
  }
`;

const CustomSwiperSlide = styled(SwiperSlide)`
  background-image: url('/statics/images/avatar-frame.png');
  background-size: 100% 100%;
  border-radius: 20px;
  div {
    background: linear-gradient(
      270deg,
      rgba(255, 0, 125, 0) 0%,
      rgba(255, 0, 125, 0.5) 49.22%,
      rgba(255, 0, 125, 0) 100%
    );
  }
`;
