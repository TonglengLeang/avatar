import Image from "next/image"
import React from "react"

interface LazyImageProps {
  src: string
  alt: string
  thumbnail?: string
  width: number
  height: number
  onClick?: () => void
  quality?: number
  className?: string
  id?: string
  style?: React.CSSProperties
  unoptimized?: boolean
}
const LazyImage = ({
  src,
  onClick,
  alt,
  thumbnail,
  style,
  width,
  height,
  quality,
  className,
  id,
  unoptimized = false,
}: LazyImageProps) => {
  const myLoader = ({ src, width, quality }) => {
    if (src?.includes("http")) {
      return `${src}?w=${width}&q=${quality || 100}`
    }
    return src
  }

  return (
    <Image
      id={id}
      src={src}
      alt={alt}
      sizes="100vw"
      loading="lazy"
      loader={myLoader}
      quality={quality || 100}
      placeholder={width < 40 ? null : "blur"}
      blurDataURL={thumbnail || src}
      width={width}
      height={height}
      onClick={onClick}
      className={className}
      style={{ maxWidth: "100%", maxHeight: "100%", ...style }}
      unoptimized={unoptimized}
    />
  )
}

export default LazyImage
