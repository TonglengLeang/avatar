import LazyImage from 'components/Image/LazyImage';
import Router from 'next/router';
import styled from 'styled-components';

const Navbar = () => {
  return (
    <Container>
      <div onClick={() => Router.back()}>
        <LazyImage
          src="/statics/images/left-arrow.png"
          width={22}
          height={22}
          alt="image"
        />
      </div>

      <RenderImageButton>Render Image</RenderImageButton>
    </Container>
  );
};
export default Navbar;

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 120px;
  padding-left: 5rem;
  padding-right: 5rem;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1;
  cursor: pointer;
`;

const RenderImageButton = styled.div`
  width: 200px;
  height: 40px;
  background-image: url('/statics/images/frame-button-pink.png');
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  background-size: 100% 100%;
`;
